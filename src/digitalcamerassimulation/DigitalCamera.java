/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcamerassimulation;

public abstract class DigitalCamera {
    
    protected String make;
    protected String model;
    protected double megapixels;
    protected int internMemorySize;
    protected int externMemorySize;
    
    public DigitalCamera(){}
    
    public DigitalCamera(String make, String model, double megapixels, 
            int memorySize){
        
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
        this.internMemorySize = memorySize;
        this.externMemorySize = memorySize;
    }
        
    public String getMake(){
        return make;
    }
    
    public void setMake(String make){
        this.make = make;
    }
    
    public String getModel(){
        return model;
    }
    
    public void setModel(String model){
        this.model = model;
    }
    
    public double getMegapixels(){
        return megapixels;
    }
    
    public void setMegapixels(double megapixels){
        this.megapixels = megapixels;
    }
    
    public int getInternMemorySize(){
        return internMemorySize;
    }
    
    public void setInternMemorySize(int internMemorySize){
        this.internMemorySize = internMemorySize;
    }
    
    public int getExternMemorySize(){
        return externMemorySize;
    }
    
    public void setExternMemorySize(int externMemorySize){
        this.externMemorySize = externMemorySize;
    }
    
    public abstract String describeCamera();

}
