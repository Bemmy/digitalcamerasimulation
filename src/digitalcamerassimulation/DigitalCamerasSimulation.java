/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcamerassimulation;

public class DigitalCamerasSimulation {

    public static void main(String[] args) {
        
        DigitalCamera camera1 = new PointAndShootCamera("Canon", 
                "Powershot A590", 8, 16);
        
        DigitalCamera camera2 = new PhoneCamera("Apple", "Iphone", 6, 64);
        
        System.out.println(camera1.describeCamera() + "\n\n" + 
                camera2.describeCamera());
    }
    
}
