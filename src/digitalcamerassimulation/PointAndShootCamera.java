/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcamerassimulation;

public class PointAndShootCamera extends DigitalCamera {
    
    public PointAndShootCamera(String make, String model, 
            double megapixels, int externMemorySize){
        
        super(make, model, megapixels, externMemorySize);     
    }
    
    
    public String describeCamera(){
        
        return this.make + "\n" + this.model + "\nMegapixels = " + 
                this.megapixels + "\nExternal memory size = " + 
                this.externMemorySize + "GB";
    }

    
}
