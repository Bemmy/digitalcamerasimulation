/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcamerassimulation;

public class PhoneCamera extends DigitalCamera {
    
        public PhoneCamera(String make, String model, double megapixels, 
                int internMemorySize){
            
            super(make, model, megapixels, internMemorySize);
        }
    
        public String describeCamera(){
        
        return this.make + "\n" + this.model + "\nMegapixels = " + 
                this.megapixels + "\nInternal memory size = " + 
                this.internMemorySize + "GB";
        }
}
